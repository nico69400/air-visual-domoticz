#!/bin/bash
# Domoticz server
SERVER="<ip_address:port>"

# DHT IDX
# le numéro de l IDX dans la liste des peripheriques
DHTIDX="<idx_number>"

#Requète pour importer dans un fichier temporaire du résultat en json
curl "http://api.airvisual.com/v1/nearest?lat=<latitude>&lon=<longitude>&key=<api_key>" > /var/tmp/pol

#On extrait la valeur qui nous intéresse
POL=$(more /var/tmp/pol | grep aqius | awk -F: '{print $2}' | awk -F"," '{print $1}')
echo $POL
polu=$(echo $POL)
curl -s -i -H "Accept: application/json" "http://<ip_address:port>/json.htm?type=command&param=udevice&idx=<idx_number>&nvalue=0&svalue=$polu"

#nettoyage
rm $TMPFILE
exit 1